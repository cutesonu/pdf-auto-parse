# PDF-Auto-Parse

## requirements

-  Code must be commented

-  Jupyter notebook

-  Requirements file

-  anaconda virtual environment would be ideal

-  export to csv, xlsx, ms sql (via insert statements)


## install requirements

    pip3 install -r requirements.txt
    

## install MSSQL dependencies
        
    sudo apt-get install unixodbc-dev
    sudo apt-get install unixodbc-bin
  
