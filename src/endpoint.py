# [START import]
import os
import sys
import pandas as pd
from src.pdf.pdf_to_img import Pdf2Img
from src.pdf.extract_data import Pdf2Data
from src.pdf.parse_xml import ParseXml
from src.template.template_utils import TemplateUtils
from src.common.common import show_page_contents
from src.export.export import save_dataframe, table_to_dataframe
from src.settings import INPUT_EXT
import src.logger as log
# [END import]


# [START utils]
pdf2img = Pdf2Img()
template_util = TemplateUtils()


#
class PDFAutoParse:
    def __init__(self, debug=False):
        self.debug = debug

    @staticmethod
    def save_page_contents(page_contents, save_path):
        import json
        with open(save_path, 'w') as jp:
            json.dump({'temp': page_contents}, jp)

    @staticmethod
    def load_page_contents(load_path):
        import json
        with open(load_path, 'r') as jp:
            page_contents = json.load(jp)
        return page_contents['temp']

    def proc(self, document_path):
        log.log_info("document: {}".format(document_path))

        # check the existence of the document =================================================================
        if not os.path.exists(document_path):
            log.log_error("no exist. exit(1)")
            sys.exit(1)

        # check the extension =================================================================================
        base, ext = os.path.splitext(os.path.split(document_path)[1])
        log.log_info("base: {}, ext: {}".format(base, ext))
        if ext.lower() not in INPUT_EXT:
            log.log_error("not supported file format. exit(1)")
            sys.exit(1)

        # extract data from pdf ===============================================================================
        log.log_info("extract data from pdf")  # using google vision API
        pdf2data = Pdf2Data()
        raw_xml_string = pdf2data.pdf_to_xml(pdf_path=document_path)
        pdf2data.release()
        xml2anno = ParseXml()
        page_contents = xml2anno.parse_xml2anno(raw_xml_string=raw_xml_string)

        if self.debug:  # show the result annotations
            # self.save_page_contents(page_contents=page_contents, save_path="temp.json")
            # page_contents = self.load_page_contents("temp.json")

            # convert pdf to images =========================
            log.log_info("convert pdf to images")
            page_img_paths = pdf2img.pdf_to_imgs(pdf_path=document_path)
            log.log_info("\t# pages: {}".format(len(page_img_paths)))

            show_page_contents(page_img_paths=page_img_paths, page_contents=page_contents)

        # parse the table from raw data =======================================================================
        table = template_util.parse(page_contents=page_contents)

        dataframe = table_to_dataframe(table=table)

        if self.debug:  # show table
            # convert pdf to images =========================
            log.log_info("show table")
            with pd.option_context('display.max_rows', None, 'display.max_columns', None):
                print(dataframe)

        # save table ==========================================================================================
        # outer_format could be one of ['csv', 'xlsx', 'mssql']
        if save_dataframe(document_path=document_path, outer_format='xlsx', dataframe=dataframe):
            log.log_error("Success!")
        else:
            log.log_error("Failed to get data!.")
            sys.exit(1)


if __name__ == '__main__':
    pap = PDFAutoParse(debug=True)
    log.log_info(">>>")

    pap.proc(document_path="..//data/signagnd_2018-10-28.pdf")
