import json
import difflib
import math
import cv2
import re
from fuzzywuzzy import fuzz

from src.annotation.anno_property import AnnoProperty
from src.static import LINE_POS, LINE_HEIGHT, LINE_ANNO_IDS
from src.static import SPACE, EMPTY
from src.static import PAGE_ID, PAGE_ANNOS, PAGE_LINES, PAGE_BBOX


anno_pro = AnnoProperty()
diff = difflib.Differ()


#
IN_SAME_GROUP = 200


def is_empty_string(text):
    return len(text.replace(SPACE, EMPTY)) == 0


def is_with_digits(text):
    text = text.replace(' ', '')
    digits = re.findall('\d+', text)
    if len(digits) != 0:
        return True
    else:
        return False


def is_digit_value(text):
    text = re.sub('[., ]', EMPTY, text)
    digits = re.findall('\d+', text)
    if len(digits) == 0:
        return False
    else:
        return len(digits[0]) == len(text)


def similarity_word(dst_str, src_str):
    ratio = float(fuzz.token_set_ratio(dst_str, src_str)) / 100.0
    avg_len = (len(dst_str) + len(src_str)) / 2
    sub_len = math.fabs(len(dst_str) - len(src_str)) / 2
    return ratio * (avg_len - sub_len) / avg_len


def equal(str1, str2):
    str1 = str1.upper().replace(" ", "")
    str2 = str2.upper().replace(" ", "")
    return similarity_word(str1, str2) > 0.9


def find_keyword(line_text, keyword):
    if len(line_text) <= len(keyword):
        ratio = float(fuzz.token_sort_ratio(line_text, keyword)) / 100.0
        max_ratio = ratio * len(line_text) / len(keyword)
        last_same_pos = 0
    else:
        max_ratio = 0
        last_same_pos = 0
        for pos in range(len(line_text) - len(keyword)):
            sub_text = line_text[pos: pos + len(keyword)]
            ratio = float(fuzz.token_sort_ratio(sub_text, keyword)) / 100.0  # instead of (fuzz.token_set_ratio)
            if max_ratio < ratio:
                max_ratio = ratio
                last_same_pos = pos

    if max_ratio > 0.9:
        return last_same_pos
    else:
        return -1


def clear_value(text):
    text = remove_double_space(text=text)
    text = re.sub('[],:[]', EMPTY, text)

    # remove the SPACE at start and end position
    _start = 0
    _end = -1

    for i in range(len(text)):
        if text[i] != SPACE:
            _start = i
            break
    for i in range(len(text) - 1, -1, -1):
        if text[i] != SPACE:
            _end = i
            break
    return text[_start:_end+1]


def get_alphabets(text):
    text = " ".join(re.findall("[a-zA-Z]+", text))
    return text


def get_digits(word):
    word = word.replace(' ', '')
    tmp = word
    tmp = tmp.replace(',', '0')
    tmp = tmp.replace('.', '0')
    digits = re.findall('\d+', tmp)
    if len(digits) == 0:
        return EMPTY

    max_di = ""
    for di in digits:
        if len(di) > len(max_di):
            max_di = di

    pos = tmp.find(max_di)
    if pos != -1:
        return word[pos: pos+len(max_di)]
    else:
        return EMPTY


def str_to_float(text):
    text = text.replace('.', '')
    text = text.replace(',', '.')
    try:
        return float(text)
    except Exception as e:
        print(e)
        return -100000.


def is_empty_text(text):
    return len(text.replace(' ', '')) == 0


def load_info(info_path):
    try:
        with open(info_path, 'r') as jp:
            info = json.load(jp)
            return info
    except Exception as e:
        print(e)
        return None


def remove_double_space(text):
    double_space = "  "
    while text.find(double_space) != -1:
        text = text.replace(double_space, " ")
    return text


def get_obj_value(line_text, keywords, sub_field=False):
    value = None
    find_pos = -1
    for keyword in keywords:
        find_pos = find_keyword(line_text=line_text, keyword=keyword)
        if find_pos != -1:
            if find_pos < len(keyword) or sub_field:
                value = clear_value(text=line_text[find_pos + len(keyword):])
            break
    return find_pos, value


def distance_between_lines(line1, line2):
    return math.fabs(line1[LINE_POS] - line2[LINE_POS])


def height_of_line(line):
    return line[LINE_HEIGHT]


def show_page_contents(page_img_paths, page_contents):
    for page in page_contents:
        page_id = page[PAGE_ID]
        page_annos = page[PAGE_ANNOS]
        page_lines = page[PAGE_LINES]
        x, y, w, h = page[PAGE_BBOX]

        print("\t  page {}".format(page_id + 1))
        print("\t  annos : {}".format(len(page_annos)))
        cv_im = cv2.imread(page_img_paths[page_id], cv2.IMREAD_COLOR)
        cv_im = cv2.resize(cv_im, (int(w), int(h)))
        for anno in page_annos:
            # print("\t\t" + anno['text'])

            text = anno['text']
            pts = anno['boundingBox']['vertices']
            x1, y1, x2, y2 = pts[0]['x'], pts[0]['y'], pts[2]['x'], pts[2]['y']

            cv2.rectangle(cv_im, (int(x1), int(y1)), (int(x2), int(y2)), (255, 0, 0), 1)
            cv2.putText(cv_im, text, (int(x1), int(y1)), cv2.FONT_HERSHEY_SIMPLEX, 0.2, (0, 0, 255), 1)

        for line in page_lines:
            # print("\t\t" + line['text'])
            indexes = line[LINE_ANNO_IDS]
            pts1 = page_annos[indexes[0]]['boundingBox']['vertices']
            pts2 = page_annos[indexes[-1]]['boundingBox']['vertices']
            x1, y1 = pts1[0]['x'], pts1[0]['y']
            x2, y2 = pts2[2]['x'], pts2[2]['y']

            cv2.rectangle(cv_im, (int(x1), int(y1)), (int(x2), int(y2)), (0, 0, 255), 1)

        # cv2.imwrite("annotations_{}.jpg".format(page_id), cv_im)
        cv2.imshow("img", cv2.resize(cv_im, None, fx=0.5, fy=0.5))
        cv2.waitKey(500)

    cv2.destroyAllWindows()
