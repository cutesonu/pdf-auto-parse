import os
from src.template.oklahoma import Oklahoma
from src.common.common import load_info, find_keyword
from src.settings import INFO_DIR, INFO_EXT
from src.static import LINE_TEXT, PAGE_LINES, KEY_PREFIX, KEY_TEMPLATE_NAME, KEY_KEYWORDS
import src.logger as log


class TemplateUtils:
    def __init__(self):
        template_info_paths = [os.path.join(INFO_DIR, fn) for fn in os.listdir(INFO_DIR)
                               if os.path.splitext(fn)[1] == INFO_EXT]
        self.temaplates = []
        for template_info_path in template_info_paths:
            self.temaplates.append(load_info(info_path=template_info_path))

    def check_template_id(self, page):
        page_lines = page[PAGE_LINES]
        template_id = -1
        for line in page_lines:
            for template_id, template in enumerate(self.temaplates):
                template_keywords = template[KEY_PREFIX][KEY_KEYWORDS]
                for keyword in template_keywords:
                    line_text = line[LINE_TEXT]
                    if find_keyword(line_text=line_text, keyword=keyword) != -1:
                        return template_id

        return template_id

    def get_template_info(self, template_id):
        return self.temaplates[template_id]

    # [MAIN]
    def parse(self, page_contents):
        # recognize the template
        first_page = page_contents[0]

        template_id = self.check_template_id(page=first_page)
        if template_id == -1:
            log.log_warn("invalid template")
            return None

        template = self.get_template_info(template_id=template_id)
        # log.log_info("template id: {}".format(template[KEY_PREFIX][KEY_TEMPLATE_NAME]))
        log.log_info("template id: {}".format(template_id))

        # parsing the template
        if template[KEY_PREFIX][KEY_TEMPLATE_NAME] == "OKLAHOMA":
            template_parser = Oklahoma(template_info=template)
        else:
            log.log_warn("invalid unknown template")
            log.log_info("TODO")
            return None

        data = template_parser.extract_data(page_contents=page_contents)

        return data
