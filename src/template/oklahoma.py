from src.static import PAGE_LINES, PAGE_ID, LINE_TEXT, LINE_ANNO_IDS, PAGE_ANNOS, EMPTY, APPENDER, SPACE
from src.static import KEY_FIELD_NAME
from src.common.common import equal, find_keyword, clear_value
from src.annotation.anno_property import AnnoProperty
# import src.logger as log

anno_pro = AnnoProperty()

FIRST_PAGE_ID = 0


class Oklahoma:
    def __init__(self, template_info, debug=False):
        self.info = template_info
        self.debug = debug

        self.page_column_pos = []
        self.title_line_text = ""
        for field in self.info["table"]:
            self.title_line_text += " " + (field['keywords'][0])

        self.footer_line_text = ""
        for field in self.info["footer"]:
            self.footer_line_text += " " + (field['keywords'][0])

    def detect_title_line(self, page):
        lines = page[PAGE_LINES]

        annos = page[PAGE_ANNOS]
        for line_id, line in enumerate(lines):
            if equal(line[LINE_TEXT], self.title_line_text):
                line_anno_ids = line[LINE_ANNO_IDS]
                temp_str = ""

                column_left_edge = 0.0
                for anno_id in line_anno_ids:
                    temp_str += " " + annos[anno_id]['text']
                    for field in self.info['table']:
                        if column_left_edge == -1:
                            column_left_edge = anno_pro.get_left_edge(anno=annos[anno_id])['x']
                        if equal(str1=temp_str, str2=field['keywords'][0]):
                            column_right_egde = anno_pro.get_right_edge(anno=annos[anno_id])['x']
                            self.page_column_pos.append([column_left_edge, column_right_egde])
                            column_left_edge = -1
                            temp_str = ""
                            break
                return line_id
        return -1

    def detect_footer_line(self, lines):
        if self.footer_line_text == EMPTY:
            return -1
        for line_id, line in enumerate(lines):
            if find_keyword(line_text=line[LINE_TEXT], keyword=self.footer_line_text) != -1:
                return line_id
        return -1

    def extract_head_data(self, lines):
        """
            BEFORE THE CORPORATION COMMISSION OF THE STATE OF OKLAHOMA
                DAILY SIGNING AGENDA (PROPOSED ORDERS)

            "docket" : DAILY SIGNING AGENDA

        """
        second_line = lines[1][LINE_TEXT]

        header_data = [{"docket": second_line.split("(")[0]}]

        for line in lines:
            for field in self.info["header"]:
                for keyword in field['keywords']:
                    pos = find_keyword(line_text=line[LINE_TEXT], keyword=keyword)
                    if pos != -1:
                        header_data.append({field[KEY_FIELD_NAME]: line[LINE_TEXT][pos + len(keyword):]})
                        break
        return header_data

    #
    def __columns_from_line(self, line, annos):
        columns = [EMPTY for i in range(len(self.page_column_pos)) if i > -1]
        line_anno_ids = line[LINE_ANNO_IDS]
        cur_column_id = 0
        while cur_column_id < len(self.page_column_pos):
            if cur_column_id == 0:
                prev_right = 0.0
                next_left = self.page_column_pos[1][0]
            elif cur_column_id == len(self.page_column_pos) - 1:
                prev_right = self.page_column_pos[cur_column_id - 1][1]
                next_left = 10000000.0
            else:
                prev_right = self.page_column_pos[cur_column_id - 1][1]
                next_left = self.page_column_pos[cur_column_id + 1][0]

            i = 0
            while i in range(len(line_anno_ids)):
                anno_id = line_anno_ids[i]
                cen_x = anno_pro.get_cen_pt(anno=annos[anno_id])['x']
                if prev_right < cen_x < next_left:
                    columns[cur_column_id] += " " + annos[anno_id]['text']
                    line_anno_ids.remove(anno_id)
                elif cen_x > next_left:
                    break
            cur_column_id += 1

        return columns

    #
    #
    def extract_table_data(self, page, start, end):
        page_items = []
        if end != -1:
            lines = page[PAGE_LINES][start: end]
        else:
            lines = page[PAGE_LINES][start:]
        annos = page[PAGE_ANNOS]

        line_id = 0
        is_start_item = False
        while line_id < len(lines):
            line1 = lines[line_id]

            if equal(line1[LINE_TEXT], self.title_line_text):
                line_id += 1
                continue
            columns1 = self.__columns_from_line(line=line1, annos=annos)
            if columns1.count("") < 2:
                is_start_item = True
                table_line = [val for val in columns1]
                page_items.append(table_line)
            elif is_start_item:
                table_line = page_items[-1]
                for i, val in enumerate(columns1):
                    if val != EMPTY:
                        table_line[i] += APPENDER + val
                page_items[-1] = table_line
            else:
                is_start_item = False
            line_id += 1
        return page_items

    def extract_data(self, page_contents):
        header_data = None
        total_items = []

        for i, page in enumerate(page_contents):

            page_lines = page[PAGE_LINES]
            page_id = page[PAGE_ID]

            self.page_column_pos.clear()
            title_line_id = self.detect_title_line(page=page)

            footer_line_id = self.detect_footer_line(lines=page_lines)

            # head data
            if page_id == FIRST_PAGE_ID or header_data is None:
                header_data = self.extract_head_data(lines=page_lines[:title_line_id])

            # items
            page_items = self.extract_table_data(page=page, start=title_line_id, end=footer_line_id)
            total_items.extend(page_items)

        return self.__rearrange(header_data=header_data, total_items=total_items)

    def __rearrange(self, header_data, total_items):
        title_line = []
        value_lines = []

        # title line
        # header_data
        for header_field in header_data:
            for key in header_field.keys():
                title_line.append(key)
        # main key fields
        for j, field in enumerate(self.info['table']):
            if j == 0:  # "Type and CauseNumber"
                title_line.append("type")
                title_line.append("cause_num")
            else:
                title_line.append(field[KEY_FIELD_NAME])

        max_len_value_line = 0
        for item in total_items:
            value_line = []
            # header values
            for header_field in header_data:
                for key in header_field.keys():
                    value_line.append(clear_value(header_field[key]))

            # main fields
            for j, field in enumerate(self.info['table']):
                if j == 0:  # "Type and CauseNumber"
                    str_type_num = clear_value(text=item[0])
                    _type = EMPTY
                    _num = EMPTY
                    if str_type_num.split(" ") == 1:
                        if len(str_type_num) > 3 and str_type_num[:2].isalpha() and str_type_num[3].isdigit():
                            _type = str_type_num[:2]
                            _num = str_type_num[2:]
                    else:  # str_type_num.split(" ") > 1
                        sps = str_type_num.split(" ")
                        if sps[0].isalpha() and sps[1].isdigit():
                            _type = sps[0]
                            _num = sps[1]
                    value_line.append(clear_value(text=_type))
                    value_line.append(clear_value(text=_num))
                else:
                    value_line.append(clear_value(text=item[j]))

            # sub order description fields
            order_descr = value_line[3 + len(header_data)]
            if order_descr != EMPTY and len(order_descr.split(APPENDER)) != 1:
                sub_descs = order_descr.split(APPENDER)
                for sub_desc in sub_descs:
                    value_line.append(clear_value(text=sub_desc))

            max_len_value_line = max(len(value_line), max_len_value_line)
            for j, value in enumerate(value_line):
                value_line[j] = value.replace(APPENDER, SPACE)

            value_lines.append(value_line)

        # update the title line with sub_order_descs
        for unique_idx, j in enumerate(range(len(title_line), max_len_value_line)):
            title_line.append("order_desc_{}".format(unique_idx + 1))

        total_lines = [title_line] + value_lines
        # fill out the empty sub_order_descr
        for i, line in enumerate(total_lines):
            if len(line) < max_len_value_line:
                for j in range(len(line), max_len_value_line):
                    total_lines[i].append(EMPTY)

        return total_lines
