import os

# paths
_cur_dir = os.path.dirname(os.path.realpath(__file__))
ROOT_DIR = os.path.abspath(os.path.join(_cur_dir, os.pardir))
print(ROOT_DIR)

LOG_DIR = os.path.join(ROOT_DIR, "logs")
INFO_DIR = os.path.join(ROOT_DIR, "dataset/templates")

# allow extensions
ALLOWED_EXT = [".pdf", ".jpg"]
INPUT_EXT = ".pdf"
OUTPUT_EXTS = ['csv', 'xlsx', 'mssql']
INFO_EXT = ".json"

# SQL settings
MSSQL_CONN = {
    "user": "sql_user",
    "password": "password",
    "server": "",
    "instance": "",
    "port": "",
    "database": ""
}
