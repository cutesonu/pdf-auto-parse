EMPTY = ''
SPACE = ' '
APPENDER = "+"


# [annotation keywords]
ANNO_TEXT = 'text'
ANNO_BBOX = 'boundingBox'
ANNO_VERTICES = 'vertices'


# [page keywords]
PAGE_ID = "page_id"
PAGE_CONTENT = "page_content"
PAGE_ROTATE = "page_rotate"
PAGE_POSITION = "position"
PAGE_BBOX = "page_bbox"
PAGE_ANNOS = "page_annos"
PAGE_LINES = "page_lines"
# in case of vision apis
PAGE_ORIENTATION = "page_orientation"
PAGE_TOTAL_TEXT = "page_total_text"
PAGE_LABEL = "page_label"

# [line keywords]
LINE_ANNO_IDS = "line_anno_ids"
LINE_POS = "line_pos"
LINE_TEXT = 'line_text'
LINE_HEIGHT = 'line_height'

# [template definition keys] rule/sections info keys
KEY_TEMPLATE_TYPE = "template_type"
KEY_PREFIX = "prefix"
KEY_TEMPLATE_NAME = "template_name"
KEY_HEADER = "header"
KEY_TABLE = "table"
KEY_FIELD_NAME = "field_name"
KEY_SUB_FIELDS = "sub_fields"
KEY_KEYWORDS = "keywords"
KEY_DESCRIPTION = "description"
KEY_ORIENTATION = "orientation"
