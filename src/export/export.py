import os
import pandas as pd
from sqlalchemy import create_engine
from src.settings import OUTPUT_EXTS
from src.settings import MSSQL_CONN

import src.logger as log


def table_to_dataframe(table):
    """
        convert the raw table data(list of list) to the dataframe of pandas
    :param table: list of list
    :return: dataframe
    """
    if table is None:
        return None
    raw_data = {}
    columns = [column for column in table[0]]
    for i, line in enumerate(table):
        if i == 0:
            for j, value in enumerate(table[i]):
                raw_data[columns[j]] = []
        else:
            for j, value in enumerate(table[i]):
                raw_data[columns[j]].append(value)

    df = pd.DataFrame(data=raw_data, columns=columns)
    return df


def save_dataframe(document_path, outer_format, dataframe):
    """

    :param document_path:
    :param outer_format: ['csv', 'xlsx', 'mssql']
    :param dataframe:
    :return:
    """
    if dataframe is None:
        return False

    base = os.path.splitext(os.path.split(document_path)[0])[0]
    if outer_format not in OUTPUT_EXTS:
        log.log_error("Output format is invalid. The extension for output file should ['csv', 'xlsx', 'mssql']")
        return False
    else:
        try:
            if outer_format == 'csv':
                output_path = os.path.splitext(document_path)[0] + '.' + outer_format
                dataframe.to_csv(output_path)
            elif outer_format == 'xlsx':
                output_path = os.path.splitext(document_path)[0] + '.' + outer_format
                writer = pd.ExcelWriter(path=output_path, engine='xlsxwriter')
                dataframe.to_excel(writer, sheet_name='Sheet1')
            elif outer_format == 'mssql':
                save_to_mssql(dataframe=dataframe, table_name=base)
                pass

            log.log_info("dataframe is saved as a {} file".format(outer_format))
            return True
        except Exception as e:
            print(e)
            return False


def save_to_mssql(dataframe, table_name):
    conn_str = "mssql+pymssql://{}\\{}/{}".format(MSSQL_CONN['server'], MSSQL_CONN['instance'], MSSQL_CONN['database'])
    connection = create_engine(conn_str)

    dataframe.to_sql(table_name, connection, if_exists='replace')  # if_exists='replace'
