import math
import copy
from operator import itemgetter

from src.static import LINE_POS, LINE_ANNO_IDS, LINE_TEXT
from src.annotation.anno_property import AnnoProperty


SAME_FONT_THRESH = 0.9  # medium
SAME_LINE_THRESH = 0.5  # small
SAME_WORD_THRESH = 0.5  # merge charactors to word 0.5 -> 0.6

MERGE_THRESH = 1.10  # small


anno_pro = AnnoProperty()


def is_same_line(anno1, anno2):
    cen_pt1 = anno_pro.get_cen_pt(anno=anno1)
    height1 = anno_pro.get_height(anno=anno1)
    cen_pt2 = anno_pro.get_cen_pt(anno=anno2)
    height2 = anno_pro.get_height(anno=anno2)
    if height1 == 0.0 or height2 == 0.0:
        return False

    line_ratio = math.fabs(float(cen_pt1['y'] - cen_pt2['y']) / float((height1 + height2) / 2.0))
    if line_ratio < SAME_LINE_THRESH:
        return True
    else:
        return False


def is_same_font_sz(anno1, anno2):
    height1 = anno_pro.get_height(anno=anno1)
    height2 = anno_pro.get_height(anno=anno2)

    if height1 / 2.0 == 0.0 or height2 / 2.0 == 0.0:
        return False

    avg_height = (height1 + height2) / 2.0
    font_sz_ratio = math.fabs(float(height1 - height2)) / avg_height

    if font_sz_ratio < SAME_FONT_THRESH:
        return True
    else:
        return False


def is_back_to_back(anno1, anno2):
    ch1 = anno_pro.get_text(anno=anno1)[-1]
    ch2 = anno_pro.get_text(anno=anno2)[0]
    ret = ch1.isalnum() and ((ch1.isupper() and ch2.islower()) or
                             (ch1.islower() and ch2.islower()) or
                             (ch1.isdigit() and ch2.isdigit())
                             )
    return ret


def is_chars_in_same_word(anno1, anno2):
    horizon_space = anno_pro.calc_space_anno2anno(anno1, anno2)['horizon']
    avg_width = (anno_pro.get_width(anno1) + anno_pro.get_width(anno2)) / 2.0
    if horizon_space / avg_width < SAME_WORD_THRESH:
        return True
    else:
        return False


def is_annos_in_same_field(anno1, anno2):
    horizon_space = anno_pro.calc_space_anno2anno(anno1, anno2)['horizon']
    avg_height = (anno_pro.get_height(anno1) + anno_pro.get_height(anno2)) / 2.0
    if is_same_line(anno1=anno1, anno2=anno2) and horizon_space / avg_height < SAME_WORD_THRESH:
        return True
    else:
        return False


def merge_characters_to_word(annos):
    word_annos = []

    cur_anno_id = 0
    to_merge_ids = []
    while cur_anno_id < len(annos) - 1:
        to_merge_ids.clear()
        to_merge_ids.append(cur_anno_id)

        for next_anno_id in range(cur_anno_id + 1, len(annos)):
            if is_chars_in_same_word(anno1=annos[to_merge_ids[-1]], anno2=annos[next_anno_id]) and \
                    is_back_to_back(anno1=annos[to_merge_ids[-1]], anno2=annos[next_anno_id]):
                to_merge_ids.append(next_anno_id)
                continue
            else:
                cur_anno_id = next_anno_id
                break

        merged_anno = anno_pro.merge_annos(annos=[annos[anno_id] for anno_id in to_merge_ids])
        word_annos.append(merged_anno)

    return word_annos


def merge_annos_to_field(annos):
    word_annos = []

    cur_anno_id = 0
    to_merge_ids = []
    while cur_anno_id < len(annos):
        to_merge_ids.clear()
        to_merge_ids.append(cur_anno_id)

        next_anno_id = cur_anno_id + 1
        for next_anno_id in range(cur_anno_id + 1, len(annos)):
            if is_annos_in_same_field(anno1=annos[to_merge_ids[-1]], anno2=annos[next_anno_id]):
                to_merge_ids.append(next_anno_id)
                continue
            else:
                break
        cur_anno_id = next_anno_id

        merged_anno = anno_pro.merge_annos(annos=[annos[anno_id] for anno_id in to_merge_ids])
        word_annos.append(merged_anno)

    return word_annos


# def line_ids2str(annos, ids_line):
#     line_str = ""
#     for id in ids_line:
#         line_str += (" " + annos[id]['text'])
#
#     return line_str
#
#
# def disline2line(line1, line2):
#     return math.fabs(line1['pos'] - line2['pos'])
#
#
# def dis_side2side(left, right):
#     end_of_left = (left['boundingBox']['vertices'][1]['x'] + left['boundingBox']['vertices'][2]['x']) / 2
#     start_of_right = (right['boundingBox']['vertices'][0]['x'] + right['boundingBox']['vertices'][3]['x']) / 2
#     return start_of_right - end_of_left
#

def __get_left_neighbor(src_anno_id, annos):
    if src_anno_id is None:
        return None
    src_ul, src_ur, src_br, src_bl = annos[src_anno_id]['boundingBox']['vertices']
    src_left_pt = {'x': (src_ul['x'] + src_bl['x']) / 2,
                   'y': (src_ul['y'] + src_bl['y']) / 2}

    min_left_dis = None
    min_left_id = None

    for anno_id in range(len(annos)):
        if anno_id == src_anno_id:
            continue

        left_r_pt = anno_pro.get_right_edge(anno=annos[anno_id])
        left_cen_pt = anno_pro.get_cen_pt(anno=annos[anno_id])

        distance = src_left_pt['x'] - left_r_pt['x']
        if left_cen_pt['x'] <= src_left_pt['x'] <= left_r_pt['x']:
            distance = 0.0

        if distance >= 0 and \
                is_same_line(annos[src_anno_id], annos[anno_id]) and \
                is_same_font_sz(annos[src_anno_id], annos[anno_id]):
            if min_left_id is None or min_left_dis > distance:  # find the minimum distance
                min_left_id = anno_id
                min_left_dis = distance

    return min_left_id, min_left_dis


def __get_right_neighbor(src_anno_id, annos):
    if src_anno_id is None:
        return None

    src_right_pt = anno_pro.get_right_edge(annos[src_anno_id])
    min_right_dis = None
    min_right_id = None

    for anno_id in range(len(annos)):
        if id == src_anno_id:
            continue

        right_cen_pt = anno_pro.get_cen_pt(anno=annos[anno_id])
        right_l_pt = anno_pro.get_left_edge(anno=annos[anno_id])

        distance = right_l_pt['x'] - src_right_pt['x']
        if right_l_pt['x'] <= src_right_pt['x'] <= right_cen_pt['x']:
            distance = 0.0

        if distance >= 0 and \
                is_same_line(annos[src_anno_id], annos[anno_id]) and \
                is_same_font_sz(annos[src_anno_id], annos[anno_id]):
            if min_right_id is None or min_right_dis > distance:
                min_right_dis = distance
                min_right_id = anno_id

    return min_right_id, min_right_dis


def __left_extends(parent_id, annos):
    left_child = __get_left_neighbor(src_anno_id=parent_id, annos=annos)[0]

    if left_child is not None:
        _lefts = __left_extends(parent_id=left_child, annos=annos)
        _lefts.extend([parent_id])
        return _lefts
    else:
        return [parent_id]


def __right_extends(parent_id, annos):
    right_child = __get_right_neighbor(src_anno_id=parent_id, annos=annos)[0]

    if right_child is not None:
        _rights = __right_extends(parent_id=right_child, annos=annos)
        _node = [parent_id]
        _node.extend(_rights)
        return _node
    else:
        return [parent_id]


def bundle_to_lines(origin_annos):
    lines = []

    annos = merge_annos_to_field(copy.deepcopy(origin_annos))
    annos_ids = list(range(len(annos)))

    while len(annos_ids) > 0:
        anno_id = annos_ids[0]
        line = []

        line.extend(__left_extends(anno_id, annos)[:])
        line.extend(__right_extends(anno_id, annos)[1:])

        idx = 0
        while idx < len(line):
            anno_id = line[idx]
            if anno_id not in annos_ids:
                line.remove(anno_id)
            else:
                idx += 1

        lines.append(line)

        # remove the ids from the entire annos ids
        sort_ids_line = line[:]
        sort_ids_line.sort()
        for i in range(len(sort_ids_line) - 1, -1, -1):
            if sort_ids_line[i] in annos_ids:
                annos_ids.remove(sort_ids_line[i])
                # del annos[sort_ids_line[i]]

    # sort the ids_lines with its position
    temp_lines = []
    for line in lines:
        fst_anno_pos = annos[line[0]]['boundingBox']['vertices']
        line_pos = (fst_anno_pos[0]['y'] + fst_anno_pos[3]['y']) / 2

        line_text = ""
        for anno_id in line:
            line_text += annos[anno_id]['text'] + ' '

        temp_lines.append({LINE_ANNO_IDS: line, LINE_POS: line_pos, LINE_TEXT: line_text})

    sorted_lines = sorted(temp_lines, key=itemgetter(LINE_POS))
    return sorted_lines, annos
